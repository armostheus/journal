const fs = require('fs');

module.exports = {
    
        "development": {
          "username": null,
          "password": null,
          "database": "main",
          "host": "localhost",
          "dialect": "sqlite",
          "storage": "database.sqlite3"
        }
      
};