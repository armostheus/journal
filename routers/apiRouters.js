const express = require('express');
const router = express.Router();
const cors = require('cors'); 
const UsersModel = require('../models/Users');
const DailyEntriesModel = require('../models/DailyEntries');
const DailySummaryModel = require('../models/DailySummary');
const NewThingsLearntModel = require('../models/NewThingsLearnt');
const bodyParser = require('body-parser');
const GoalsModel = require('../models/Goals');
const CommentsModel = require('../models/Comments');
const Sequelize = require('sequelize');

const Op = Sequelize.Op;
var jsonParser = bodyParser.json()

var urlencodedParser = bodyParser.urlencoded({ extended: false })
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite3'
}); 

router.use(cors());
router.use('/search/:word', (req,res,next)=>{
    word = req.params['word'];
    DailyEntriesModel.findAll({
        attributes: ['entry_id', 'title', 'description'],
        where: {
            [Op.or]:[{
                title: {
                    [Op.like]: '%'+word+'%'
                }
            },
            {
                description: {
                    [Op.like]: '%'+word+'%'
                }
            }               
            ]
        }
    }).then(entries => {
        req.resultset=[];
        req.resultset.push(entries);
        console.log(entries);
        console.log(req.resultset+' 1st fn ');
        next();
    })
    
}, (req,res,next) => {
    word = req.params['word'];
    DailySummaryModel.findAll({
        attributes: ['summ_id', 'title', 'desc'],
        where: {
            [Op.or]:[{
                title: {
                    [Op.like]: '%'+word+'%'
                }
            },
            {
                desc: {
                    [Op.like]: '%'+word+'%'
                }
            }               
            ]
        }
    }).then(summaries => {
        req.resultset.push(summaries);
        console.log(summaries);
        console.log(req.resultset+' 2nd fn');
        next();
    })
}, (req,res,next) => {
    word = req.params['word'];
    GoalsModel.findAll({
        attributes: ['goal_id', 'goal'],
        where: {
            goal: {
                [Op.like]: '%'+word+'%'
            }
        }
    }).then(goals => {
        req.resultset.push(goals);
        console.log(goals);
        console.log(req.resultset+' 3rd fn');
        next();
    })
}, (req,res,next) => {
    word = req.params['word'];
    CommentsModel.findAll({
        attributes: ['comment_id', 'comment'],
        where: {
            comment: {
                [Op.like]: '%'+word+'%'
            }
        }
    }).then(comments => {
        req.resultset.push(comments);
        console.log(comments);
        console.log(req.resultset+' 4th fn');
        next();
    })
});
/* router.use()
 */

router.get('/testRoute', (req,res)=>{
    res.send('Pinging you back')
});

router.get('/users/:userId', (req,res)=>{

    if(req.params['userId'] != null){
      UsersModel.findByPk(req.params['userId'])
      .then(user => {
        res.send(JSON.stringify(user))
      });
    } else {
      if(req.params['userNm'] != null){
        UsersModel.findByPk(req.params['userId'])
        .then(user => {
          res.send(JSON.stringify(user))
        });
      }
    }
});

router.post('/users/', urlencodedParser, (req, res)=>{
  UsersModel.create({ first_name: req.body.first_name, last_name: req.body.last_name, username: req.body.username })
    .then(user => {
      res.send(user)
    })
});

router.post('/logEntry/', urlencodedParser, (req,res)=>{
  console.log(req.body);
  DailyEntriesModel.create({userUserId: req.body.userUserId, title:req.body.title, description:req.body.description, mood:req.body.mood, star:req.body.star, sad:req.body.sad, media:req.body.media, media_caption:req.body.media_caption})
  .then(entry => {
    res.send(entry)
  })
});

router.get('/logEntry/:startDate.:endDate', urlencodedParser, (req,res)=>{
/*   DailyEntriesModel.findAll({
    where:{
      createdAt: {
        [Op.between]: [
          req.params['startDate'],
          req.params['endDate']
        ]
      }   
    }
  })
  .then(entries => {
    res.send(entries)
  });  */
  sequelize
  .query(`SELECT * FROM daily_entries where createdAt >= '${req.params['startDate']}' and createdAt <= '${req.params['endDate']}'`, {
    model: DailyEntriesModel
  })
  .then(entries => {
    res.send(entries)
  })
});

router.post('/logSummary/', urlencodedParser, (req,res)=>{ 
  DailySummaryModel.create({userUserId: req.body.userUserId, title:req.body.title, desc:req.body.description, how_was_day:req.body.how_was_day, media:req.body.media, media_caption:req.body.media_caption})
  .then(entry => {
    res.send(entry)
  })
});

router.get('/logSummary/', urlencodedParser, (req,res)=>{
  DailySummaryModel.findAll()
  .then(entries => {
    res.send(entries)
  });
});

router.post('/comment/', urlencodedParser, (req,res)=>{ 
  CommentsModel.create({userUserId: req.body.userUserId, up_link_id:req.body.up_link_id, comment:req.body.comment, media:req.body.media, media_caption:req.body.media_caption})
  .then(entry => {
    res.send(entry)
  })
});

router.get('/search/:word', urlencodedParser, (req,res)=>{
    console.log(req.resultset+' main router');
    res.send(JSON.stringify(req.resultset));
});

router.get('/memorableDays/:start_dt.:end_dt', urlencodedParser, (req,res)=>{
  //search for every star marked entriesfrom start date to end date
  DailyEntriesModel.findAll({
    attributes : ['entry_id', 'title', 'description'],
    where: {
      star: true,
      createdAt: {
        [Op.gte]: req.params['start_dt'],
        [Op.lte]: req.params['end_dt']
      }
    }
  }).then(memories => res.send(JSON.stringify(memories)))
});

router.get('/sadDays/:start_dt.:end_dt', urlencodedParser, (req,res)=>{
  //search for every sad marked entriesfrom start date to end date
  DailyEntriesModel.findAll({
    attributes : ['entry_id', 'title', 'description'],
    where: {
      sad: true,
      createdAt: {
        [Op.gte]: req.params['start_dt'],
        [Op.lte]: req.params['end_dt']
      }
    }
  }).then(sad => res.send(JSON.stringify(sad)))
});

router.post('/whatyoulearnt/', urlencodedParser, (req,res)=>{
  NewThingsLearntModel.create({userUserId: req.body.userUserId, lesson: req.body.lesson})
  .then(learning => {
    res.send(learning)
  })
});

router.get('/whatyoulearnt/:start_dt.:end_dt', urlencodedParser, (req,res)=>{
  //search for what you have learnt from start date to end date
  NewThingsLearntModel.findAll({
    attributes : ['id', 'lesson'],
    where: {
      createdAt: {
        [Op.gte]: req.params['start_dt'],
        [Op.lte]: req.params['end_dt']
      }
    }
  }).then(memories => res.send(JSON.stringify(memories)))
});

router.post('/goals/', urlencodedParser, (req,res)=>{
  GoalsModel.create({userUserId: req.body.userUserId, goal_start_date: req.body.goal_start_date, goal_end_date: req.body.goal_end_date, goal: req.body.goal, goal_importance: req.body.goal_importance, goal_editable: req.body.goal_editable, goal_completion_date: req.body.goal_completion_date})
  .then(goals => {
    res.send(goals)
  })
});

router.get('/goals/:start_dt.:end_dt', urlencodedParser, (req,res)=>{
  //search for your goals from start date to end date
  GoalsModel.findAll({
    attributes : ['goal_id', 'goal'],
    where: {
      createdAt: {
        [Op.gte]: req.params['start_dt'],
        [Op.lte]: req.params['end_dt']
      }
    }
  }).then(memories => res.send(JSON.stringify(memories)))
});

module.exports = router;