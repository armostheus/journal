const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

class DailySummary extends Sequelize.Model {}
DailySummary.init({
  // attributes
  summ_id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  desc: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  how_was_day: {
    type: Sequelize.INTEGER,
    defaultValue: 3,
    allowNull: false,
    validate: {
      isIn: [[1,2,3,4,5]]
    }
  },
  media: {
    type: Sequelize.BLOB
  },
  media_caption: {
    type: Sequelize.TEXT
  }
}, {
  sequelize,
  modelName: 'daily_summary'
  // options
});

module.exports = DailySummary;