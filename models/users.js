const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

class Users extends Sequelize.Model {}
Users.init({
  // attributes
  user_id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true
  },
  first_name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  last_name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false
  } 
 }, {
  sequelize,
  modelName: 'users'
  // options
});

module.exports = Users;