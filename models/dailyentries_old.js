'use strict';
module.exports = (sequelize, DataTypes) => {
  const DailyEntries = sequelize.define('DailyEntries', {
    user_id: DataTypes.INTEGER,
    entry_date: DataTypes.DATE,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    star: DataTypes.BOOLEAN,
    sad: DataTypes.BOOLEAN
  }, {});
  DailyEntries.associate = function(models) {
    // associations can be defined here
  };
  return DailyEntries;
};