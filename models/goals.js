const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

class Goals extends Sequelize.Model {}
Goals.init({
  // attributes
  goal_id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  goal_start_date: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW
  },
  goal_end_date: {
    type: Sequelize.DATE
  },
  goal: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  goal_importance: {
    type: Sequelize.INTEGER,
    defaultValue: 3,
    allowNull: false,
    validate: {
      isIn: [[1,2,3,4,5]]
    }
  },
  goal_editable: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
  goal_completion_date: {
    type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'goals'
  // options
});

module.exports = Goals;