const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

  class DailyEntries extends Sequelize.Model {}
DailyEntries.init({
  // attributes
  entry_id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT
  },
  mood:{
    type: Sequelize.INTEGER,
    defaultValue: 3,
    allowNull: true,
    validate: {
      isIn: [[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]]
    }
  },
  star: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  sad: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  media: {
    type: Sequelize.BLOB
  },
  media_caption: {
    type: Sequelize.TEXT
  }
}, {
  sequelize,
  modelName: 'daily_entries'
  // options
});

module.exports = DailyEntries;