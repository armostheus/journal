const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

  class Comments extends Sequelize.Model {}
Comments.init({
  // attributes
  comment_id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  up_link_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  comment: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  media: {
    type: Sequelize.BLOB
  },
  media_caption: {
    type: Sequelize.TEXT
  }
}, {
  sequelize,
  modelName: 'comments'
  // options
});

module.exports = Comments;