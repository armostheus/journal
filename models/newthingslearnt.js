const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

class NewThingsLearnt extends Sequelize.Model {}
NewThingsLearnt.init({
  // attributes
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true
  },
  lesson: {
    type: Sequelize.TEXT,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'new_things_learnt'
  // options
});


module.exports = NewThingsLearnt;