const express = require('express'); 
const Sequelize = require('sequelize');
//import express from 'express';
const cors = require('cors'); 
const bodyParser = require('body-parser')
const env = require('dotenv/config');
const sqlite3 = require('sqlite3').verbose();
const apiRoutes = require('../routers/apiRouters');
const UsersModel = require('../models/Users');
const DailyEntriesModel = require('../models/DailyEntries');
const DailySummaryModel = require('../models/DailySummary');
const NewThingsLearntModel = require('../models/NewThingsLearnt');
const CommentsModel = require('../models/Comments');
const GoalsModel = require('../models/Goals');
/* let db = new sqlite3.Database('./database.sqlite3', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
}); */

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use('/dbAPI',apiRoutes);

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite3'
  }); 

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

//Declaring sequelize models
const Model = Sequelize.Model;

UsersModel.hasMany(DailyEntriesModel);
UsersModel.hasMany(DailySummaryModel);
DailySummaryModel.hasMany(NewThingsLearntModel);
UsersModel.hasMany(CommentsModel);
UsersModel.hasMany(GoalsModel);

sequelize.sync(); 



app.get('/', (req,res) => {
    res.send('Hello World!');
});

app.listen(3100, () => 
    console.log(`Example app listening to 
                3100`),
);
